local Engine = require('engine')

local LoseRendering = Engine.System.newType()

function LoseRendering.new(entities)
  return Engine.System.new(
    LoseRendering,
    entities,
    {}
  )
end

function LoseRendering:draw()
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  local lifetime = entity:get(Engine.Components.Lifetime)

  local w = love.window.getWidth()
  local h = love.window.getHeight()
  local x = 0
  local y = h / 2 - 72 / 2 - 20

  love.graphics.setFont(love.graphics.newFont(72))
  love.graphics.printf('GAME OVER', 0, y, w, 'center')

  love.graphics.setFont(love.graphics.newFont(24))

  local time = string.format('You lasted for %i seconds before dying', lifetime.value)
  love.graphics.printf(time, 0, y + 140, w, 'center')

  love.graphics.printf('[ Press space to go again ]', 0, y + 300, w, 'center')
end

return LoseRendering
