local Engine = require('engine')

local LoseSpace = {}

local LoseRenderingSystem = require('systems.lose_rendering')
local LoseInputSystem = require('systems.lose_input')

function LoseSpace.new(entities)
  local systems = {
    LoseRenderingSystem.new(entities),
    LoseInputSystem.new(entities),
  }

  return Engine.Space.new(entities, systems)
end

return LoseSpace
