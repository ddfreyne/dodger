local Engine = require('engine')
local Gamestate = require('engine.vendor.hump.gamestate')

local LoseInput = {}
LoseInput.__index = LoseInput

function LoseInput.new(entities)
  return setmetatable({ entities = entities }, LoseInput)
end

function LoseInput:keypressed(key, isrepeat)
  if key == " " then
    local MainState = require('gamestates.main')
    Gamestate.switch(MainState.new())
    return
  end
end

return LoseInput
