local Engine = require('engine')
local Components = require('components')

local Offscreen = Engine.System.newType()

function Offscreen.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
  }

  return Engine.System.new(Offscreen, entities, requiredComponentTypes)
end

function Offscreen:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)

  if position.y > love.window.getHeight() then
    self.entities:remove(entity)
  end
end

return Offscreen
