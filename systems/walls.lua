local Engine = require('engine')
local Components = require('components')

local Walls = Engine.System.newType()

function Walls.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
    Engine.Components.Velocity,
    Components.ScreenConstraint,
  }

  return Engine.System.new(Walls, entities, requiredComponentTypes)
end

function Walls:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)

  if position.x < 0 then
    position.x = 0
  end

  if position.x > love.window.getWidth() then
    position.x = love.window.getWidth()
  end

  if position.y < 0 then
    position.y = 0
  end

  if position.y > love.window.getHeight() then
    position.y = love.window.getHeight()
  end
end

return Walls
