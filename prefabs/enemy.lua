local Engine     = require('engine')
local Components = require('components')

local Asteroid = {}

function Asteroid.new()
  local self = Engine.Entity.new()

  local imagePath = 'assets/Space shooter/Meteors/meteorBrown_small1.png'

  self:add(Engine.Components.Description,    'Asteroid')
  self:add(Engine.Components.Velocity,       0, 0)
  self:add(Engine.Components.Scale,          1)
  self:add(Engine.Components.Rotation,       0)
  self:add(Engine.Components.Z,              0)
  self:add(Engine.Components.Image,          imagePath)
  self:add(Engine.Components.CollisionGroup, 'enemy')
  self:add(Components.Health,                1)

  return self
end

return Asteroid
