local ArenaSpace = require('spaces.arena')
local Player     = require('prefabs.player')
local Components = require('components')

local Engine   = require('engine')

local lw = love.window

local Main = {}

local function createEntities()
  local entities = Engine.Types.EntitiesCollection.new()

  local camera = Engine.Entity.new()
  camera:add(Engine.Components.Camera)
  camera:add(Engine.Components.Position, lw.getWidth()/2, lw.getHeight()/2)
  camera:add(Engine.Components.Z, 100)
  entities:add(camera)

  local player = Player.new()
  player:add(Engine.Components.Position, love.window.getWidth() / 2, love.window.getHeight() - 50)
  entities:add(player)

  local spawner = Engine.Entity.new()
  spawner:add(Components.Spawner)
  entities:add(spawner)

  return entities
end

function Main.new()
  local entities = createEntities()
  local arenaSpace = ArenaSpace.new(entities)

  return Engine.Gamestate.new({ arenaSpace })
end

return Main
