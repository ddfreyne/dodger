local Engine = require('engine')
local Debugger = require('debugger')

local Gamestate = require('engine.vendor.hump.gamestate')

local Components = require('components')

local Input = {}
Input.__index = Input

function Input.new(entities)
  return setmetatable({ entities = entities }, Input)
end

function Input:update(dt)
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  if not entity then return end

  local positionComponent = entity:get(Engine.Components.Position)
  if love.keyboard.isDown("left") then
    positionComponent.x = positionComponent.x - (600 * dt)
  elseif love.keyboard.isDown("right") then
    positionComponent.x = positionComponent.x + (600 * dt)
  end
end

function Input:keypressed(key, isrepeat)
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  if not entity then return end

  -- Open debugger
  if key == "tab" then
    Gamestate.push(Debugger.Gamestate.new(self.entities))
    return
  end
end

return Input
