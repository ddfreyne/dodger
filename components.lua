require('engine')

local Components = {}

Components.Health = {
  order  = 0,
  name   = 'Health',
  new    = function(max) return { cur = max, max = max } end,
  format = function(self) return '' .. self.cur .. '/' .. self.max end,
}

Components.Strength = {
  order  = 1,
  name   = 'Strength',
  new    = function(s) return { value = s } end,
  format = function(self) return '' .. self.value end,
}

Components.Allegiance = {
  order  = 3,
  name   = 'Allegiance',
  new    = function(value) return { value = value } end,
  format = function(self) return '' .. self.value end,
}

Components.Collideable = {
  order  = 4,
  name   = 'Can collide?',
  new    = function() return {} end,
  format = function(self) return 'yes' end,
}

Components.Bullet = {
  order  = 5,
  name   = 'Is bullet?',
  new    = function() return {} end,
  format = function(self) return 'yes' end,
}

Components.Gun = {
  order  = 6,
  name   = 'Gun',
  new    = function(rate, cooldown, isShooting, bulletPrefab) return { rate = rate, cooldown = cooldown, isShooting = isShooting, bulletPrefab = bulletPrefab } end,
  format = function(self) return (self.isShooting and 'on' or 'off') .. '; rate: ' .. string.format('%3.1f', self.rate) .. '; ' .. 'cooldown: ' .. string.format('%3.1f', self.cooldown) end,
}

Components.Split = {
  order  = 15,
  name   = 'Splits on collision?',
  new    = function(size) return { size = size } end,
  format = function(self) return 'Yes; size = ' .. self.size end,
}

Components.MaxLifetime = {
  order  = 16,
  name   = 'Max lifetime',
  new    = function(value) return { value = value } end,
  format = function(self) return '' .. self.value end,
}

Components.ScreenConstraint = {
  order  = 17,
  name   = 'Constrained to screen?',
  new    = function() return {} end,
  format = function(self) return 'Yes' end,
}

Components.Spawner = {
  order  = 18,
  name   = 'Spawner',
  new    = function(rate) return { rate = 0.3, rateChange = 0.05, cooldown = 0, velocity = 300, acceleration = 10 } end,
  format = function(self) return 'Rate: ' .. self.rate .. '; cooldown: ' .. self.cooldown end,
}

return Components
