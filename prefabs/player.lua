local Engine     = require('engine')
local Components = require('components')

local Player = {}

function Player.new()
  local self = Engine.Entity.new()

  local imagePath = 'assets/Space shooter/playerShip1_blue_FIXED.png'

  self:add(Engine.Components.Input           )
  self:add(Engine.Components.Description,    'Player')
  self:add(Engine.Components.Velocity,       0, 0)
  self:add(Engine.Components.Scale,          0.5)
  self:add(Engine.Components.Rotation,       -math.pi/2)
  self:add(Engine.Components.Z,              0)
  self:add(Engine.Components.Lifetime        )
  self:add(Engine.Components.Image,          imagePath)
  self:add(Engine.Components.CollisionGroup, 'player')
  self:add(Components.Health,                1)
  self:add(Components.ScreenConstraint       )

  return self
end

return Player
