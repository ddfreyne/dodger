local Engine = require('engine')
local Components = require('components')
local Gamestate  = require('engine.vendor.hump.gamestate')
local LoseState = require('gamestates.lose')

local CollisionHandler = Engine.System.newType()

function CollisionHandler.new(entities)
  return Engine.System.new(
    CollisionHandler,
    entities,
    {},
    { Engine.Systems.CollisionDetection.signal }
  )
end

function CollisionHandler:handleSignal(name, attributes)
  if name == Engine.Systems.CollisionDetection.signal then
    self:pairDetected(attributes.a, attributes.b)
  end
end

function CollisionHandler:pairDetected(a, b)
  self:singleDetected(a, b)
end

function CollisionHandler:singleDetected(entity, otherEntity)
  local health        = entity:get(Components.Health)
  local position      = entity:get(Engine.Components.Position)
  local split         = entity:get(Components.Split)

  local otherVelocity = otherEntity:get(Engine.Components.Velocity)
  local otherPosition = otherEntity:get(Engine.Components.Position)

  health.cur = health.cur - 1
  if health.cur <= 0 then
    Gamestate.switch(LoseState.new(self.entities))
  end
end

return CollisionHandler
