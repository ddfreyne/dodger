local Engine     = require('engine')
local Components = require('components')
local Enemy      = require('prefabs.enemy')

local Spawner = Engine.System.newType()

function Spawner.new(entities)
  local requiredComponentTypes = {
    Components.Spawner,
  }

  return Engine.System.new(Spawner, entities, requiredComponentTypes)
end

function Spawner:updateEntity(entity, dt)
  local spawner = entity:get(Components.Spawner)

  if spawner.cooldown < 0 then
    spawner.cooldown = spawner.cooldown + spawner.rate

    local enemy = Enemy.new()
    enemy:add(Engine.Components.Position, love.math.random(0, love.window.getWidth()), 0)
    enemy:add(Engine.Components.Velocity, 0, spawner.velocity)
    self.entities:add(enemy)
  end

  spawner.rate     = spawner.rate * (1.0 - spawner.rateChange * dt)
  spawner.velocity = spawner.velocity + spawner.acceleration * dt

  spawner.cooldown = spawner.cooldown - dt
end

return Spawner
