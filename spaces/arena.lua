local InputSystem    = require('systems.input')
local WallsSystem    = require('systems.walls')
local CollisionHandlingSystem    = require('systems.collision_handling')
local SpawnerSystem    = require('systems.spawner')
local OffscreenSystem    = require('systems.offscreen')

local Engine = require('engine')

local Arena = {}

function Arena.new(entities)
  local systems = {
    InputSystem.new(entities),
    Engine.Systems.CollisionDetection.new(entities),
    CollisionHandlingSystem.new(entities),
    WallsSystem.new(entities),
    OffscreenSystem.new(entities),
    SpawnerSystem.new(entities),
    Engine.Systems.Physics.new(entities),
    Engine.Systems.Rendering.new(entities),
    Engine.Systems.Lifetime.new(entities),
  }

  return Engine.Space.new(entities, systems)
end

return Arena
