local Engine = require('engine')

local LoseSpace = require('spaces.lose')

local Lose = {}

function Lose.new(entities)
  local loseSpace = LoseSpace.new(entities)

  return Engine.Gamestate.new({ loseSpace })
end

return Lose
