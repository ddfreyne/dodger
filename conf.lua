function love.conf(t)
  t.title = "Dodger"
  t.window.width = 1024
  t.window.height = 768
end

package.path = package.path .. ';vendor/d-tech/?.lua;vendor/d-tech/?/init.lua'
